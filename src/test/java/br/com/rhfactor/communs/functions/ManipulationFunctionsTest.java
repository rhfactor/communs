package br.com.rhfactor.communs.functions;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import br.com.rhfactor.communs.beans.CurrencyBeanTest;

public class ManipulationFunctionsTest {

	@Test
	public void getAllFieldsTest() {
		LinkedList<Field> fields = new LinkedList<Field>();
		ManipulationFunctions.getAllFields(fields, CurrencyBeanTest.class);
		assertNotNull(fields.size());
		assertTrue(fields.size() > 0);
	}

	@Test
	public void getAllFieldsAsStringTest() {
		List<String> fields = ManipulationFunctions.getAllFieldsAsString(CurrencyBeanTest.class);
		assertNotNull(fields.size());
		assertTrue(fields.size() > 0);
		for (String field : fields) {
			System.out.println(field);
		}
		assertTrue(fields.contains("serialVersionUID"));
	}

}
