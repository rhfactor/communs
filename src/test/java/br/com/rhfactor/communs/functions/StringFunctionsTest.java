package br.com.rhfactor.communs.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringFunctionsTest {
	
	@Test
	public void testUppercaseFirstLetter(){
		
		assertEquals( StringFunctions.uppercaseFirst("Humanidade").substring(0, 1) , "H" );
		assertNotEquals( StringFunctions.uppercaseFirst("Humanidade").substring(0, 1) , "h" );
		assertEquals( StringFunctions.uppercaseFirst("Amanhã").substring(0, 1) , "A" );
		assertNotEquals( StringFunctions.uppercaseFirst("Amanhã").substring(0, 1) , "a" );
		
	}

	@Test
	public void generateNumericRandomTest() {
		for (int i = 1; i <= 1000; i++) {
			assertEquals(i, StringFunctions.generateNumericRandom(i).length());
		}
	}
	
	@Test
	public void isValidImageExtensionTest(){

		assertFalse( StringFunctions.isValidImageExtension(".jp") );
		assertFalse( StringFunctions.isValidImageExtension("jp") );
		assertFalse( StringFunctions.isValidImageExtension("bpm") );
		assertFalse( StringFunctions.isValidImageExtension(".bpm") );
		assertFalse( StringFunctions.isValidImageExtension("pdf") );
		assertFalse( StringFunctions.isValidImageExtension(".pdf") );
		assertTrue( StringFunctions.isValidImageExtension("jpg") );
		assertTrue( StringFunctions.isValidImageExtension("png") );
		assertTrue( StringFunctions.isValidImageExtension("gif") );
		assertTrue( StringFunctions.isValidImageExtension("bmp") );
		assertTrue( StringFunctions.isValidImageExtension("svg") );
		assertTrue( StringFunctions.isValidImageExtension(".jpg") );
		assertTrue( StringFunctions.isValidImageExtension(".png") );
		assertTrue( StringFunctions.isValidImageExtension(".gif") );
		assertTrue( StringFunctions.isValidImageExtension(".bmp") );
		assertTrue( StringFunctions.isValidImageExtension(".svg") );
		
	}
	
	@Test
	public void generateRandomWithParameters(){
		assertEquals(StringFunctions.generateRandom(50).length(),50);
		assertEquals(StringFunctions.generateRandom(150).length(),150);
		assertEquals(StringFunctions.generateRandom(200).length(),200);
		assertEquals(StringFunctions.generateRandom(2000).length(),2000);
		assertEquals(StringFunctions.generateRandom(5000).length(),5000);
	}

}
