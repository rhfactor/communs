package br.com.rhfactor.communs.validators;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.rhfactor.communs.validators.URLContextValidator.URLContext;

public class URLContextValidatorTest {
	
final static Logger logger = Logger.getLogger(URLContextValidatorTest.class);
	
	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	private static Validator validator;

	class SpacedContextClass {
		
		@URLContext
		public String spaceFirst = " alo";
		
		@URLContext
		public String spaceAfter = "alo ";
		
		@URLContext
		public String spaceInBetween = "alo alo";
		
		@URLContext
		public String lessThanFive = "alo";
		
		@URLContext
		public String specialCharacteres = "alo+oi[]\\/()";
		
	}
	
	class ExpectedValuesClass {
		
		@URLContext
		public String atLeastFive = "alo-Alo";
		
		@URLContext
		public String withCharacteres = "alo_alo-";
		
	}


	@BeforeClass
	public static void setUpClass() throws Exception {
		validator = factory.getValidator();
	}
	
	// Testes com valores mínimos

	@Test
	public void testarClassesComEspaco() {
		SpacedContextClass min = new SpacedContextClass();
		Set<ConstraintViolation<SpacedContextClass>> violations = validator.validate(min);
		assertEquals(5, violations.size());
	}
	
	@Test
	public void testarClasseCorreta() {
		ExpectedValuesClass min = new ExpectedValuesClass();
		Set<ConstraintViolation<ExpectedValuesClass>> violations = validator.validate(min);
		assertEquals(0, violations.size());
	}

}
