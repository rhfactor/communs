package br.com.rhfactor.communs.validators;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import br.com.rhfactor.communs.validators.BigDecimalValuesValidator.BigDecimalValues;

/**
 * <h1>BigDecimalMinValidatorTest</h1>
 * <h2>Ver {@link BigDecimalValuesValidator}</h2>
 * 
 * <p>
 * Teste unitário para validação de bean validator
 * </p>
 * 
 * @author Roberto Fonseca
 *
 */
public class BigDecimalMinValidatorTest {

	final static Logger logger = Logger.getLogger(BigDecimalMinValidatorTest.class);
	
	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	private static Validator validator;

	class MinDecimalValueBean {
		@BigDecimalValues(min = 1.0)
		public BigDecimal value = null;
	}

	class MaxDecimalValueBean {
		@BigDecimalValues(max = 10.0)
		public BigDecimal value = null;
	}

	class MinMaxDecimalValueBean {
		@BigDecimalValues(max = 10.0, min = 3.0)
		public BigDecimal value = null;
	}
	
	class RealCase {
		@BigDecimalValues(min = 5000.00)
		private BigDecimal maxAllocationSize;
		
		@BigDecimalValues(min = 5000)
		private BigDecimal setUpFee;
		
		@BigDecimalValues(min = 500)
		private BigDecimal monthlyFee;
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
		validator = factory.getValidator();
	}
	
	// Testar RealCase
	@Test
	public void testarCasoReal(){
		RealCase min = new RealCase();
		
		// Testar quando o valor é menor que o mínimo
		
		min.maxAllocationSize = new BigDecimal("3.0");
		min.setUpFee = new BigDecimal("3.0");
		min.monthlyFee = new BigDecimal("3.0");
		
		Set<ConstraintViolation<RealCase>> violations = validator.validate(min);
		assertEquals(3, violations.size());
		
		// Testar quando o valor é próximo ao mínimo
		
		min.maxAllocationSize = new BigDecimal("4999");
		min.setUpFee = new BigDecimal("4999");
		min.monthlyFee = new BigDecimal("499");
		
		Set<ConstraintViolation<RealCase>> violations2 = validator.validate(min);
		assertEquals(3, violations2.size());
		
		// Testar quando o valor é maior que o mínimo
		
		min.maxAllocationSize = new BigDecimal("6000");
		min.setUpFee = new BigDecimal("6000");
		min.monthlyFee = new BigDecimal("600");
		
		Set<ConstraintViolation<RealCase>> violations3 = validator.validate(min);
		assertEquals( 0, violations3.size());
		
		// Testar quando o valor é igual ao mínimo
		
		min.maxAllocationSize = new BigDecimal("5000");
		min.setUpFee = new BigDecimal("5000");
		min.monthlyFee = new BigDecimal("500");
		
		Set<ConstraintViolation<RealCase>> violations4 = validator.validate(min);
		assertEquals( 0, violations4.size());
		
	}
	

	// Testes com valores mínimos

	@Test
	public void testarSeOValorMinimoNaoEhSuperado() {
		MinDecimalValueBean min = new MinDecimalValueBean();
		min.value = new BigDecimal("3.0"); // O valor do bean é 3, e o mínimo é
											// 1, não tem uma violação de mínimo
		Set<ConstraintViolation<MinDecimalValueBean>> violations = validator.validate(min);
		assertEquals(0, violations.size());
	}

	@Test
	public void testarSeOValorMinimoEhSuperado() {
		MinDecimalValueBean min = new MinDecimalValueBean();
		min.value = new BigDecimal("0.5"); // O valor do bean é 0.5, e o mínimo
											// é 1, tem uma violação de mínimo
		Set<ConstraintViolation<MinDecimalValueBean>> violations = validator.validate(min);
		assertEquals(1, violations.size());
	}

	// Testar máximas

	@Test
	public void testarSeOValorMaximoEhSuperado() {
		MaxDecimalValueBean max = new MaxDecimalValueBean();
		max.value = new BigDecimal("10.5"); // O valor do bean é 10.50, e o
											// máximo é 10, tem uma violação de
											// mínimo
		Set<ConstraintViolation<MaxDecimalValueBean>> violations = validator.validate(max);
		assertEquals(1, violations.size());
	}

	@Test
	public void testarSeOValorMaximoNaoEhSuperado() {
		MaxDecimalValueBean max = new MaxDecimalValueBean();
		max.value = new BigDecimal("9.5"); // O valor do bean é 9.50, e o máximo
											// é 10, não tem uma violação de
											// mínimo
		Set<ConstraintViolation<MaxDecimalValueBean>> violations = validator.validate(max);
		assertEquals(0, violations.size());
	}

	// Testar com ambos os valores

	@Test
	public void valorEstahAcimaDoRequisitado() {
		MinMaxDecimalValueBean minMax = new MinMaxDecimalValueBean();
		minMax.value = new BigDecimal("10.5"); // O valor do bean é 10.50, está
												// entre 10 e 3, sim, tem erro
		Set<ConstraintViolation<MinMaxDecimalValueBean>> violations = validator.validate(minMax);
		assertEquals(1, violations.size());
	}

	@Test
	public void valorEstahEntreORequesitado() {
		MinMaxDecimalValueBean minMax = new MinMaxDecimalValueBean();
		minMax.value = new BigDecimal("9.5"); // O valor do bean é 9.50, está
												// entre 10 e 3, sim, não tem
												// erro
		Set<ConstraintViolation<MinMaxDecimalValueBean>> violations = validator.validate(minMax);
		assertEquals(0, violations.size());
	}

	@Test
	public void valorEstahAbaixoDoRequisitado() {
		MinMaxDecimalValueBean minMax = new MinMaxDecimalValueBean();
		minMax.value = new BigDecimal("2.5"); // O valor do bean é 2.50, está
												// entre 10 e 3, não, sim tem
												// erro
		Set<ConstraintViolation<MinMaxDecimalValueBean>> violations = validator.validate(minMax);
		assertEquals(1, violations.size());
	}
}
