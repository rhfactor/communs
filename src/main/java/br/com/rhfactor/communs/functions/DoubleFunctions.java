package br.com.rhfactor.communs.functions;

import java.util.Random;

import org.apache.log4j.Logger;
/**
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 */
public class DoubleFunctions {

	final static Logger logger = Logger.getLogger(DoubleFunctions.class);

	public static double generateRandom(double rangeMin, double rangeMax) {
		Random r = new Random();
		double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();

		return randomValue;
	}

}
