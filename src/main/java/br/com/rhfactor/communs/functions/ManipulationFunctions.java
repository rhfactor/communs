package br.com.rhfactor.communs.functions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 */
public class ManipulationFunctions {

	public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
		fields.addAll(Arrays.asList(type.getDeclaredFields()));
		if (type.getSuperclass() != null) {
			fields = getAllFields(fields, type.getSuperclass());
		}
		return fields;
	}

	public static List<String> getAllFieldsAsString(Class<?> type) {
		List<String> fields = new ArrayList<String>();
		for (Field field : getAllFields(new LinkedList<Field>(), type)) {
			fields.add(field.getName());
		}
		return fields;
	}

}
