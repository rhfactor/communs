package br.com.rhfactor.communs.functions;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 */
public class StringFunctions {

	final static Logger logger = Logger.getLogger(StringFunctions.class);

	private static final String IMAGE_PATTERN = "((\\.?)(jpg|png|gif|bmp|svg|jpeg)$)";
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Generates a Random String
	 * 
	 * @return random string
	 */
	public static String generateRandom() {
		return new BigInteger(130, new SecureRandom()).toString(32);
	}

	/**
	 * @param size for string length
	 * @return random string with specified size
	 */
	public static String generateNumericRandom(final Integer size) {

		logger.info("Size: " + size);
		String num = "";
		while (num.length() < size) {
			num += new BigInteger(size * 4, new SecureRandom()).toString();
		}
		return num.substring(0, size);
	}

	/**
	 * Generates a Random String in especific size
	 * 
	 * @param size for string length
	 * @return random string with specified size
	 */
	public static String generateRandom(Integer size) {
		String phrase = "";

		while (phrase.length() < size) {
			phrase += StringFunctions.generateRandom();
		}

		return phrase.substring(0, size);
	}

	/**
	 * Convert a text to Hash
	 * 
	 * @param text to convert
	 * @return hash string
	 * @throws NoSuchAlgorithmException when can't find Algorithm SHA-256
	 * @throws UnsupportedEncodingException when can't find Encoding URF-8
	 */
	public static String hash(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigest[] = algorithm.digest(text.getBytes("UTF-8"));

		StringBuilder hexString = new StringBuilder();
		for (byte b : messageDigest) {
			hexString.append(String.format("%02X", 0xFF & b));
		}

		return hexString.toString();
	}

	/**
	 * Validate if email is correct
	 * 
	 * @param email string
	 * @return true if is valid
	 */
	public static boolean isValidEmail(final String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	/**
	 * Validate if an extention is image
	 * 
	 * @param extention you want to valid image
	 * @return if is valid
	 */
	public static boolean isValidImageExtension(final String extention) {
		Pattern pattern = Pattern.compile(IMAGE_PATTERN);
		Matcher matcher = pattern.matcher(extention);
		return matcher.matches();
	}

	/**
	 * Encrypt a phrase
	 * 
	 * @param text input
	 * @param securityKey for converting
	 * @return hashed key
	 * @throws UnsupportedEncodingException could not find the requested encoding
	 * @throws NoSuchAlgorithmException the algorithm you're trying to use is invalid
	 * @throws NoSuchPaddingException could not find the algorithm 
	 * @throws InvalidKeyException key informed is invalid
	 * @throws IllegalBlockSizeException illegal block size
	 * @throws GeneralSecurityException general security
	 */
	public static String encrypt(String text, String securityKey)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, GeneralSecurityException {
		Key key = generateKey(securityKey);
		Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(text.getBytes());
		return new Base64().encodeToString(encVal);

	}

	/**
	 * Decrypt a phrase
	 * 
	 * @param text hashed text you want to decrypt
	 * @param securityKey your security key for decrypt
	 * @return decripted text
	 * @throws UnsupportedEncodingException could not find the requested encoding
	 * @throws NoSuchAlgorithmException the algorithm you're trying to use is invalid
	 * @throws NoSuchPaddingException could not find the algorithm 
	 * @throws InvalidKeyException key informed is invalid
	 * @throws IllegalBlockSizeException something you've typed is unexpected
	 * @throws BadPaddingException could not find the algorigthm
	 */
	public static String decrypt(String text, String securityKey)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(securityKey);
		Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new Base64().decode(text);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	/**
	 * Generate a securityKey using a phrase
	 * 
	 * @param securityKey
	 * @return generated key
	 * @throws WrongDataException cant't convert from string
	 * @throws UnsupportedEncodingException when can't find Encoding URF-8
	 */
	private static Key generateKey(String securityKey) throws UnsupportedEncodingException {
		if (securityKey.length() != 16) {
			throw new RuntimeException("{invalid.security.key}");
		}
		Key key = new SecretKeySpec(securityKey.getBytes("UTF-8"), "AES");
		return key;
	}
	
	/**
	 * Generate file name
	 * 
	 * @param fileName to get extension
	 * @return file and extension
	 */
	public static String generateFileName(String fileName) {
		return generateRandom(20) + "." + FilenameUtils.getExtension(fileName);
	}
	
	/**
	 * Capitalize String
	 * 
	 * @param text to capitalize
	 * @return Capitalized string
	 */
	public static String uppercaseFirst(String text) {
		String f = text.substring(0, 1).toUpperCase();
		return f + text.substring(1);
	}

}
