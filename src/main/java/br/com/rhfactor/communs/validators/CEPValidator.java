package br.com.rhfactor.communs.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.regex.Pattern;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import br.com.rhfactor.communs.validators.CEPValidator.CEP;

/** 
 * Braziliam ZipCode Format Validation
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 */
public class CEPValidator implements ConstraintValidator<CEP, String> {

	private Pattern pattern = Pattern.compile("[0-9]{5}-[0-9]{3}");

	@Override
	public void initialize(CEP constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return pattern.matcher(value).matches();
	}

	@Constraint(validatedBy = CEPValidator.class)
	@Documented
	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface CEP {

		String message() default "{invalid.cep}";

		Class<?>[] groups() default {};

		Class<? extends Payload>[] payload() default {};

	}

}
