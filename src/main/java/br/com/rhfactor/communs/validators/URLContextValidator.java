package br.com.rhfactor.communs.validators;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.apache.log4j.Logger;

import br.com.rhfactor.communs.validators.URLContextValidator.URLContext;

/**
 * Valid Suffix to compose URL 
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 */
public class URLContextValidator implements ConstraintValidator<URLContext, String>  {
	
	final static Logger logger = Logger.getLogger(URLContextValidator.class);
	final String REGEX_PATTERN = "^([a-zA-Z0-9\\_\\-]){5,}$";
	
	@Override
	public void initialize(URLContext annotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean retorno = false;
		logger.info("O valor: " + value + " contém pelo menos 5 caracteres" );
		if( value == null || ( value != null && value.length() <= 4 ) ){
			return retorno;
		}
		return value.matches(REGEX_PATTERN);
	}
	
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
	@Retention(RUNTIME)
	@Documented
	@Constraint(validatedBy = URLContextValidator.class)
	public @interface URLContext {
		String message() default "invalid.url.path";
		Class<?>[] groups() default {};
		Class<? extends Payload>[] payload() default {};
	}


}
