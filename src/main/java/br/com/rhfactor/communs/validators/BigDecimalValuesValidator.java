package br.com.rhfactor.communs.validators;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.math.BigDecimal;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.apache.log4j.Logger;

import br.com.rhfactor.communs.validators.BigDecimalValuesValidator.BigDecimalValues;

/**
 * Validation for BigDecimal
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 */
public class BigDecimalValuesValidator implements ConstraintValidator<BigDecimalValues, BigDecimal> {

	final static Logger logger = Logger.getLogger(BigDecimalValuesValidator.class);

	private Double min;
	private Double max;

	@Override
	public void initialize(BigDecimalValues annotation) {
		this.min = annotation.min();
		this.max = annotation.max();
	}

	@Override
	public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {

		boolean retorno = false;

		if (value == null) {
			return retorno;
		}

		logger.info("O valor: " + value + " é menor que o máximo " + max + " e maior do que o mínio " + min);

		return (value.doubleValue() <= max) && (value.doubleValue() >= min);
	}

	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
	@Retention(RUNTIME)
	@Documented
	@Constraint(validatedBy = BigDecimalValuesValidator.class)
	public @interface BigDecimalValues {

		double min() default Double.MIN_VALUE;

		double max() default Double.MAX_VALUE;

		String message() default "{value.doesnt.match}";

		Class<?>[] groups() default {};

		Class<? extends Payload>[] payload() default {};
	}

}
