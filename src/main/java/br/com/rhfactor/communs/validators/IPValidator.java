package br.com.rhfactor.communs.validators;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.regex.Pattern;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.apache.log4j.Logger;

import br.com.rhfactor.communs.validators.IPValidator.IP;

/**
 * IP Validation
 * 
 * @author Roberto Fonseca Alves - roberto@rhfactor.com.br
 *
 */
public class IPValidator implements ConstraintValidator<IP, String> {

	final static Logger logger = Logger.getLogger(IPValidator.class);
	
	private static final String IPADDRESS_PATTERN =
			"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	private Pattern pattern;
	private boolean required;
	

	@Override
	public void initialize(IP annotation) {
		pattern = Pattern.compile(IPADDRESS_PATTERN);
		required = annotation.required();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if( this.required && value==null || value.length() == 0 ){
			return false;
		}
		return pattern.matcher(value).matches();
	}

	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
	@Retention(RUNTIME)
	@Documented
	@Constraint(validatedBy = IPValidator.class)
	public @interface IP {
		
		boolean required() default false;

		String message() default "Wrong IP Address";

		Class<?>[] groups() default {};

		Class<? extends Payload>[] payload() default {};

		
	}

}
